from search import *


def solve_brute_force(start_state, max_movements):
    for i, v in enumerate(start_state):
        if 0 in v:
            m = i
            n = v.index(0)

    start_node = Node(start_state, position(m, n), 0)
    open_list = [start_node]

    while len(open_list) != 0:
        node = open_list.pop()

        if node.movements > max_movements:
            continue

        if node.is_solution():
            return node
        else:
            children = node.get_children()
            children.sort(reverse=False)
            open_list.extend(children)
    return None


def delete_greater_movements_nodes(children, min_movements):
    res = []
    for node in children:
        if node.movements < min_movements:
            res.append(node)
    return res


def solve_branch_and_bound(start_state, max_movements):
    for i, v in enumerate(start_state):
        if 0 in v:
            m = i
            n = v.index(0)

    start_node = Node(start_state, position(m, n), 0)
    open_list = [start_node]

    min_cost_node = None

    while len(open_list) != 0:
        node = open_list.pop()

        if node.movements > max_movements:
            continue

        if node.is_solution():
            if min_cost_node is None:
                min_cost_node = node
            else:
                if min_cost_node.movements > node.movements:
                    min_cost_node = node
        else:
            children = node.get_children()
            children.sort(reverse=False)
            if len(children) != 0 and min_cost_node is not None:
                children = delete_greater_movements_nodes(children, min_cost_node.movements)
            open_list.extend(children)
    return min_cost_node


def get_solution(solution_node):
    if solution_node is None:
        return
    list_result = [solution_node.state]
    node = solution_node
    while node.parent is not None:
        list_result.append(node.parent.state)
        node = node.parent
    list_result.reverse()
    return list_result
