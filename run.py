import solver


def run_test():
    import search_test;
    search_test.IsSolutionNodeTest.run()
    search_test.GetChildNodeTest.run()
    search_test.NumSortedPiecesTest.run()
    search_test.LowerThanTest.run()
    search_test.SortTest.run()


start_state = [[7, 4, 8],
               [6, 5, 3],
               [2, 0, 1]]


def run_solver_complete_solution():
    solution = solver.get_solution(solver.solve_branch_and_bound(start_state, 30))
    step = 0
    if solution is not None:
        for state in solution:
            print(step)
            step += 1
            for v in state:
                print(v)


def run_solver_short_solution():
    solution = solver.solve_branch_and_bound(start_state, 20)
    print(solution.movements)
    for v in solution.state:
        print(v)


if __name__ == "__main__":
    run_solver_complete_solution()
