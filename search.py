
from collections import *

position = namedtuple('position', 'i j')

solution = [[1, 2, 3],
            [4, 5, 6],
            [7, 8, 0]]

solution_zero_position = position(2, 2)


def exchange(matrix, position_a, position_b):
    temp = matrix[position_a.i][position_a.j]
    matrix[position_a.i][position_a.j] = matrix[position_b.i][position_b.j]
    matrix[position_b.i][position_b.j] = temp


class Node:
    def __init__(self, state, zero_position, movements, parent=None):
        """

        :param state: puzzle state
        :param zero_position: zero location
        :param parent: parent node
        """
        self.state = state
        self.parent = parent
        self.zero_position = zero_position
        self.movements = movements

    def is_solution(self):
        if self.zero_position == solution_zero_position:
            for i, e in enumerate(solution):
                for j, f in enumerate(e):
                    if self.state[i][j] != f:
                        return False
            return True
        return False

    def get_num_sorted_pieces(self):
        count = 0
        num = 1
        for v in self.state:
            for e in v:
                if e == num:
                    count = count + 1
                num = num + 1
        if self.state[2][2] == 0:
            count += 1
        return count

    @staticmethod
    def __corner(state, zero_position, parent):
        children = []
        if zero_position == (0, 0):
            child1 = [row[:] for row in state]
            child2 = [row[:] for row in state]
            exchange(child1, zero_position, position(0, 1))
            exchange(child2, zero_position, position(1, 0))
            children.append(Node(child1, position(0, 1), parent.movements+1, parent=parent))
            children.append(Node(child2, position(1, 0), parent.movements+1, parent=parent))
            return children
        elif zero_position == (0, 2):
            child1 = [row[:] for row in state]
            child2 = [row[:] for row in state]
            exchange(child1, zero_position, position(0, 1))
            exchange(child2, zero_position, position(1, 2))
            children.append(Node(child1, position(0, 1), parent.movements+1, parent=parent))
            children.append(Node(child2, position(1, 2), parent.movements+1, parent=parent))
            return children
        elif zero_position == (2, 0):
            child1 = [row[:] for row in state]
            child2 = [row[:] for row in state]
            exchange(child1, zero_position, position(1, 0))
            exchange(child2, zero_position, position(2, 1))
            children.append(Node(child1, position(1, 0), parent.movements+1, parent=parent))
            children.append(Node(child2, position(2, 1), parent.movements+1, parent=parent))
            return children
        elif zero_position == (2, 2):
            child1 = [row[:] for row in state]
            child2 = [row[:] for row in state]
            exchange(child1, zero_position, position(1, 2))
            exchange(child2, zero_position, position(2, 1))
            children.append(Node(child1, position(1, 2), parent.movements+1, parent=parent))
            children.append(Node(child2, position(2, 1), parent.movements+1, parent=parent))
            return children
        return children

    @staticmethod
    def __edges(state, zero_position, parent):
        children = []
        if zero_position == (0, 1):
            child1 = [row[:] for row in state]
            child2 = [row[:] for row in state]
            child3 = [row[:] for row in state]
            exchange(child1, zero_position, position(0, 0))
            exchange(child2, zero_position, position(1, 1))
            exchange(child3, zero_position, position(0, 2))
            children.append(Node(child1, position(0, 0), parent.movements+1, parent=parent))
            children.append(Node(child2, position(1, 1), parent.movements+1, parent=parent))
            children.append(Node(child3, position(0, 2), parent.movements+1, parent=parent))
            return children
        elif zero_position == (1, 0):
            child1 = [row[:] for row in state]
            child2 = [row[:] for row in state]
            child3 = [row[:] for row in state]
            exchange(child1, zero_position, position(0, 0))
            exchange(child2, zero_position, position(1, 1))
            exchange(child3, zero_position, position(2, 0))
            children.append(Node(child1, position(0, 0), parent.movements+1, parent=parent))
            children.append(Node(child2, position(1, 1), parent.movements+1, parent=parent))
            children.append(Node(child3, position(2, 0), parent.movements+1, parent=parent))
            return children
        elif zero_position == (1, 2):
            child1 = [row[:] for row in state]
            child2 = [row[:] for row in state]
            child3 = [row[:] for row in state]
            exchange(child1, zero_position, position(0, 2))
            exchange(child2, zero_position, position(1, 1))
            exchange(child3, zero_position, position(2, 2))
            children.append(Node(child1, position(0, 2), parent.movements+1, parent=parent))
            children.append(Node(child2, position(1, 1), parent.movements+1, parent=parent))
            children.append(Node(child3, position(2, 2), parent.movements+1, parent=parent))
            return children
        elif zero_position == (2, 1):
            child1 = [row[:] for row in state]
            child2 = [row[:] for row in state]
            child3 = [row[:] for row in state]
            exchange(child1, zero_position, position(2, 0))
            exchange(child2, zero_position, position(1, 1))
            exchange(child3, zero_position, position(2, 2))
            children.append(Node(child1, position(2, 0), parent.movements+1, parent=parent))
            children.append(Node(child2, position(1, 1), parent.movements+1, parent=parent))
            children.append(Node(child3, position(2, 2), parent.movements+1, parent=parent))
            return children
        return children

    @staticmethod
    def __center(state, zero_position, parent):
        children = []
        child1 = [row[:] for row in state]
        child2 = [row[:] for row in state]
        child3 = [row[:] for row in state]
        child4 = [row[:] for row in state]
        exchange(child1, zero_position, position(0, 1))
        exchange(child2, zero_position, position(1, 0))
        exchange(child3, zero_position, position(1, 2))
        exchange(child4, zero_position, position(2, 1))
        children.append(Node(child1, position(0, 1), parent.movements+1, parent=parent))
        children.append(Node(child2, position(1, 0), parent.movements+1, parent=parent))
        children.append(Node(child3, position(1, 2), parent.movements+1, parent=parent))
        children.append(Node(child4, position(2, 1), parent.movements+1, parent=parent))
        return children

    @staticmethod
    def __delete_grandparent_node(children, grandparent_node):
        for n in children:
            if n == grandparent_node:
                children.remove(n)

    def __lt__(self, other):
        return self.get_num_sorted_pieces() < other.get_num_sorted_pieces()

    def __eq__(self, other):
        if other is None:
            return False
        if self.zero_position != other.zero_position:
            return False
        if len(self.state) != len(other.state):
            return False
        for m, n in zip(self.state, other.state):
            if len(m) != len(n):
                return False
        for m, n in zip(self.state, other.state):
            for p, q in zip(m, n):
                if p != q:
                    return False
        return True

    def get_children(self):
        """
        Calculates childrens
        :return: children of self node. Answer does not include grandparent node
        """
        children = []
        if self.zero_position == (0, 0) or self.zero_position == (0, 2) or\
                self.zero_position == (2, 0) or self.zero_position == (2, 2):  # Corners
            children = Node.__corner(self.state, self.zero_position, self)
        if self.zero_position == (0, 1) or self.zero_position == (1, 0) or\
                self.zero_position == (1, 2) or self.zero_position == (2, 1):  # Edges
            children = Node.__edges(self.state, self.zero_position, self)
        if self.zero_position == (1, 1):
            children = Node.__center(self.state, self.zero_position, self)
        if self.parent is not None:
            self.__delete_grandparent_node(children, self.parent)
        return children

