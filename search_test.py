from search import *


class GetChildNodeTest:

    @staticmethod
    def run():
        GetChildNodeTest.left_upper_corner_test()
        GetChildNodeTest.right_upper_corner_test()
        GetChildNodeTest.left_bottom_corner_test()
        GetChildNodeTest.right_bottom_corner_test()
        GetChildNodeTest.upper_edge_test()
        GetChildNodeTest.bottom_edge_test()
        GetChildNodeTest.left_edge_test()
        GetChildNodeTest.right_edge_test()
        GetChildNodeTest.center_test()
        print("GetChildNodeTest : 9 test runned")

    @staticmethod
    def left_upper_corner_test():
        m = [[0, 1, 2],
             [4, 5, 3],
             [7, 8, 6]]
        node = Node(m, position(0, 0), 0)
        children = node.get_children()
        assert len(children) == 2
        assert children[0].state == [[1, 0, 2],
                                     [4, 5, 3],
                                     [7, 8, 6]] or \
               children[1].state == [[1, 0, 2],
                                     [4, 5, 3],
                                     [7, 8, 6]]
        assert children[0].state == [[4, 1, 2],
                                     [0, 5, 3],
                                     [7, 8, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [0, 5, 3],
                                     [7, 8, 6]]

    @staticmethod
    def right_upper_corner_test():
        m = [[1, 2, 0],
             [4, 5, 3],
             [7, 8, 6]]
        node = Node(m, position(0, 2), 0)
        children = node.get_children()
        assert len(children) == 2
        assert children[0].state == [[1, 0, 2],
                                     [4, 5, 3],
                                     [7, 8, 6]] or \
               children[1].state == [[1, 0, 2],
                                     [4, 5, 3],
                                     [7, 8, 6]]
        assert children[0].state == [[1, 2, 3],
                                     [4, 5, 0],
                                     [7, 8, 6]] or \
               children[1].state == [[1, 2, 3],
                                     [4, 5, 0],
                                     [7, 8, 6]]

    @staticmethod
    def left_bottom_corner_test():
        m = [[4, 1, 2],
             [7, 5, 3],
             [0, 8, 6]]
        node = Node(m, position(2, 0), 0)
        children = node.get_children()
        assert len(children) == 2
        assert children[0].state == [[4, 1, 2],
                                     [0, 5, 3],
                                     [7, 8, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [0, 5, 3],
                                     [7, 8, 6]]
        assert children[0].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 0, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 0, 6]]

    @staticmethod
    def right_bottom_corner_test():
        m = [[4, 1, 2],
             [7, 5, 3],
             [8, 6, 0]]
        node = Node(m, position(2, 2), 0)
        children = node.get_children()
        assert len(children) == 2
        assert children[0].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 0, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 0, 6]]
        assert children[0].state == [[4, 1, 2],
                                     [7, 5, 0],
                                     [8, 6, 3]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 5, 0],
                                     [8, 6, 3]]

    @staticmethod
    def upper_edge_test():
        m = [[4, 0, 2],
             [7, 1, 3],
             [8, 5, 6]]
        node = Node(m, position(0, 1), 0)
        children = node.get_children()
        assert len(children) == 3
        assert children[0].state == [[0, 4, 2],
                                     [7, 1, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[0, 4, 2],
                                     [7, 1, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[0, 4, 2],
                                     [7, 1, 3],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 2, 0],
                                     [7, 1, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 2, 0],
                                     [7, 1, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 2, 0],
                                     [7, 1, 3],
                                     [8, 5, 6]]


    @staticmethod
    def bottom_edge_test():
        m = [[4, 1, 2],
             [7, 5, 3],
             [8, 0, 6]]
        node = Node(m, position(2, 1), 0)
        children = node.get_children()
        assert len(children) == 3
        assert children[0].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [0, 8, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [0, 8, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [0, 8, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 6, 0]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 6, 0]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 6, 0]]

    @staticmethod
    def left_edge_test():
        m = [[4, 1, 2],
             [0, 7, 3],
             [8, 5, 6]]
        node = Node(m, position(1, 0), 0)
        children = node.get_children()
        assert len(children) == 3
        assert children[0].state == [[0, 1, 2],
                                     [4, 7, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[0, 1, 2],
                                     [4, 7, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[0, 1, 2],
                                     [4, 7, 3],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [8, 7, 3],
                                     [0, 5, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [8, 7, 3],
                                     [0, 5, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [8, 7, 3],
                                     [0, 5, 6]]

    @staticmethod
    def right_edge_test():
        m = [[4, 1, 2],
             [7, 3, 0],
             [8, 5, 6]]
        node = Node(m, position(1, 2), 0)
        children = node.get_children()
        assert len(children) == 3
        assert children[0].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 0, 3],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 1, 0],
                                     [7, 3, 2],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 1, 0],
                                     [7, 3, 2],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 1, 0],
                                     [7, 3, 2],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [7, 3, 6],
                                     [8, 5, 0]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 3, 6],
                                     [8, 5, 0]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 3, 6],
                                     [8, 5, 0]]

    @staticmethod
    def center_test():
        m = [[4, 1, 2],
             [7, 0, 3],
             [8, 5, 6]]
        node = Node(m, position(1, 1), 0)
        children = node.get_children()
        assert len(children) == 4
        assert children[0].state == [[4, 0, 2],
                                     [7, 1, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 0, 2],
                                     [7, 1, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 0, 2],
                                     [7, 1, 3],
                                     [8, 5, 6]] or \
               children[3].state == [[4, 0, 2],
                                     [7, 1, 3],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [0, 7, 3],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [0, 7, 3],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [0, 7, 3],
                                     [8, 5, 6]] or \
               children[3].state == [[4, 1, 2],
                                     [0, 7, 3],
                                     [8, 5, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 0, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 0, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 0, 6]] or \
               children[3].state == [[4, 1, 2],
                                     [7, 5, 3],
                                     [8, 0, 6]]

        assert children[0].state == [[4, 1, 2],
                                     [7, 3, 0],
                                     [8, 5, 6]] or \
               children[1].state == [[4, 1, 2],
                                     [7, 3, 0],
                                     [8, 5, 6]] or \
               children[2].state == [[4, 1, 2],
                                     [7, 3, 0],
                                     [8, 5, 6]] or \
               children[3].state == [[4, 1, 2],
                                     [7, 3, 0],
                                     [8, 5, 6]]


class IsSolutionNodeTest:

    @staticmethod
    def run():
        IsSolutionNodeTest.solution_node_test()
        IsSolutionNodeTest.no_solution_node_test()
        IsSolutionNodeTest.permuted_solution_node_test()
        print("IsSolutionNodeTest : 3 test runned")

    @staticmethod
    def solution_node_test():
        s1 = [[2, 0, 3],
              [1, 4, 6],
              [7, 5, 8]]

        n1 = Node(s1, position(0, 1), 0)
        assert not n1.is_solution()

    @staticmethod
    def no_solution_node_test():
        s2 = [[1, 2, 3],
              [4, 5, 6],
              [7, 8, 0]]

        n2 = Node(s2, position(2, 2), 0)
        assert n2.is_solution()

    @staticmethod
    def permuted_solution_node_test():
        s3 = [[1, 2, 3],
              [4, 8, 5],
              [7, 6, 0]]

        n3 = Node(s3, position(0, 1), 0)
        assert not n3.is_solution()


class NumSortedPiecesTest:

    @staticmethod
    def run():
        NumSortedPiecesTest.solution_test()
        NumSortedPiecesTest.no_solution_test()
        print("NumSortedPiecesTest: 2 test runned")

    @staticmethod
    def solution_test():
        state = [[1, 2, 3],
                 [4, 5, 6],
                 [7, 8, 0]]
        n = Node(state, position(2,2), 0)
        assert n.get_num_sorted_pieces() == 9

    @staticmethod
    def no_solution_test():
        state = [[1, 5, 2],
                 [4, 0, 3],
                 [7, 8, 6]]
        n = Node(state, position(1, 1), 0)
        assert n.get_num_sorted_pieces() == 4


class LowerThanTest:
    @staticmethod
    def run():
        state1 = [[1, 2, 3],
                 [4, 5, 6],
                 [7, 8, 0]]
        n1 = Node(state1, position(2, 2), 0)
        state2 = [[1, 5, 2],
                 [4, 0, 3],
                 [7, 8, 6]]
        n2 = Node(state2, position(1, 1), 0)
        assert n1 > n2
        print("LowerThanTest: 1 test runned")


class SortTest:
    @staticmethod
    def run():
        state1 = [[1, 2, 3],
                  [4, 5, 6],
                  [7, 8, 0]]
        n1 = Node(state1, position(2, 2), 0)
        state2 = [[1, 5, 2],
                  [4, 0, 3],
                  [7, 8, 6]]
        n2 = Node(state2, position(1, 1), 0)
        lista = [n1, n2]
        lista.sort(reverse=False)
        assert lista[0].state == state2
        assert lista[1].state == state1
        for e in lista:
            print(e.state)
        print("SortTest: 1 test runned")
